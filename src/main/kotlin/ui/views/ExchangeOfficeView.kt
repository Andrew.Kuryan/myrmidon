package ui.views

import logic.ExchangeOffice
import tornadofx.*

class ExchangeOfficeView(office: ExchangeOffice) : View("My View") {

    override val root = anchorpane {
        hbox {
            vbox {
                label(office.name)
                if (office.address != null) {
                    label(office.address)
                }
            }
            gridpane {
                row {
                    label("Валюта")
                    label("Покупка")
                    label("Продажа")
                }
                for (currency in office.currencies) {
                    row {
                        label(currency.type.toString())
                        label(currency.purchase.toString())
                        label(currency.selling.toString())
                    }
                }
            }
        }
    }
}
