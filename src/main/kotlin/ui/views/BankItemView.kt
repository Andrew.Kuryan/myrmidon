package ui.views

import javafx.collections.ObservableList
import javafx.scene.layout.AnchorPane
import logic.AbstractBankSite
import tornadofx.*
import ui.Controller
import ui.styles.BankItemStyle

class BankItemView(bankSite: AbstractBankSite,
                   offices: ObservableList<AnchorPane>,
                   filter: (name: String) -> Boolean) : View("My View") {

    override val root = anchorpane {
        addClass(BankItemStyle.bankItemViewRoot)
        label(bankSite.name) {
            anchorpaneConstraints {
                topAnchor = 0.0
                rightAnchor = 0.0
                bottomAnchor = 0.0
                leftAnchor = 0.0
            }
        }
        setOnMouseClicked {
            Controller.onBankItemClick(bankSite, offices, filter)
        }
    }
}
