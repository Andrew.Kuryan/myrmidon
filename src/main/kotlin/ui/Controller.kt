package ui

import javafx.application.Platform
import javafx.collections.ObservableList
import javafx.scene.layout.AnchorPane
import logic.AbstractBankSite
import logic.web_view_sites.IdeaBankSite
import ui.views.ExchangeOfficeView

object Controller {

    fun onBankItemClick(bankSite: AbstractBankSite,
                        offices: ObservableList<AnchorPane>,
                        filter: (name: String) -> Boolean) {
        offices.clear()
        if (bankSite !is IdeaBankSite) {
            Thread {
                bankSite.loadFilteredExchangeData(filter) {
                    Platform.runLater {
                        offices.add(ExchangeOfficeView(it).root)
                    }
                }
            }.start()
        } else {
            bankSite.loadFilteredExchangeData(filter) {
                Platform.runLater {
                    offices.add(ExchangeOfficeView(it).root)
                }
            }
        }
    }
}