package ui.styles

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class BankItemStyle: Stylesheet() {

    companion object {
        val bankItemViewRoot by cssclass()
    }

    init {
        bankItemViewRoot {
            prefWidth = 300.px; prefHeight = 100.px
        }
    }
}