package ui.styles

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class ExchangeOfficeStyle: Stylesheet() {

    companion object {
        val exchangeOfficeViewRoot by cssclass()
    }

    init {
        exchangeOfficeViewRoot {
            prefWidth = 800.px; prefHeight = 150.px
        }
    }
}