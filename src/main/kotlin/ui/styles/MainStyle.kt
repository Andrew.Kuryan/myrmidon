package ui.styles

import tornadofx.Stylesheet
import tornadofx.cssclass
import tornadofx.px

class MainStyle: Stylesheet() {

    companion object {
        val mainViewRoot by cssclass()
        val leftNav by cssclass()
        val rightList by cssclass()
    }

    init {
        mainViewRoot {
            prefWidth = 1000.px; prefHeight = 650.px
            leftNav {
                prefWidth = 300.px
            }
            rightList {
                prefWidth = 700.px
            }
            separator {

            }
        }
    }
}