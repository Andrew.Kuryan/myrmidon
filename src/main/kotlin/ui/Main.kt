package ui

import javafx.collections.FXCollections
import javafx.scene.layout.AnchorPane
import logicports.factories.*
import tornadofx.*
import tornadofx.App
import ui.styles.MainStyle
import ui.views.BankItemView

class Myrmidon: App(MainView::class)

class MainView: View("Myrmidon"){

    private val availableFactories =
        arrayListOf(
            BNBSiteFactory(),
            FransabankSiteFactory(),
            RRBSiteFactory(),
            MMBSiteFactory(),
            IdeaBankSiteFactory()
        )

    private val filters = arrayListOf(
        {name: String -> name == "Пункт обмена валют №6 (ТЦ «Секрет»)"},
        {name: String -> name == "ПОВ №27" || name == "ПОВ №28" || name == "ПОВ №30"},
        {name: String -> name == "Гомель, Пункт обмена валют №3/1 ЦБУ №3"},
        {name: String -> name == "ОК №427"},
        {name: String -> name == "РКЦ №31"}
    )

    private val offices = FXCollections.observableArrayList<AnchorPane>()

    init {
        importStylesheet<MainStyle>()
    }

    override val root = borderpane {
        addClass(MainStyle.mainViewRoot)
        left = listview<AnchorPane> {
            addClass(MainStyle.leftNav)
            val list = FXCollections.observableArrayList<AnchorPane>()
            for ((i, factory) in availableFactories.withIndex()) {
                list.add(BankItemView(factory.createBankSite(), offices, filters[i]).root)
            }
            items = list
        }
        center = listview<AnchorPane> {
            addClass(MainStyle.rightList)
            items = offices
        }
    }
}