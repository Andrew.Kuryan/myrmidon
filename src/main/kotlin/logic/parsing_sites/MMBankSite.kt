package logic.parsing_sites

import logic.*
import org.jsoup.nodes.Element

class MMBankSite: AbstractBankSite {

    companion object {
        const val baseUrl = "https://www.mmbank.by/ajax/coursecomp.php"
    }

    override val name = "Банк Дабрабыт"

    fun createUrl(vararg params: String): String {
        return "$baseUrl?cityName=${params[0]}&otdel=${params[1]}&sale=&sale_input=100&cPrISO=BYN&cRsISO=BYN&itog=&time="
    }

    private fun Element.parseExchangeCourses(): ArrayList<CurrencyCourse> {
        val courses = arrayListOf<CurrencyCourse>()
        val items = this.getElementsByClass("courses-table")[0]
                        .getElementsByTag("tr")
        for (item in items) {
            val cells = item.getElementsByTag("td")
            if (cells.size == 5) {
                val course = CurrencyCourse(
                    cells[0].ownText().getCourseType(),
                    if (cells[3].ownText().isNotEmpty()) {cells[3].ownText().toDouble()} else {0.0},
                    if (cells[4].ownText().isNotEmpty()) {cells[4].ownText().toDouble()} else {0.0}
                )
                courses += course
            }
        }
        return courses
    }

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) {
        val list = mutableMapOf<Pair<String, String>, ArrayList<Triple<String, String, String>>>()
        val doc = loadPage("https://www.mmbank.by/currency_exchange/")
        //val lines = Files.readAllLines(Paths.get("src/test/resources/mmbtest.html"))
        //val doc = Jsoup.parse(lines.joinToString("\n"))

        //форма для ввода обменника
        val form = doc.getElementsByClass("filter-row")[0]
        //элемент select со списком городов
        val optionsCities = form.getElementsByClass("filter-row__el")[0]
            .getElementsByTag("option")
        for (option in optionsCities) {
            //аттрибут value элемента option
            val value = option.attr("value")
            if (value.isNotEmpty()) {
                list += (value to option.text()) to arrayListOf()
            }
        }
        for (city in list.keys) {
            //запрос формы для каждого города в списке
            val ajaxDoc = loadPage(createUrl(city.first, ""))
            //элемент select со списком обменников
            val optionsOffices = ajaxDoc.getElementsByClass("filter-row__el--mra")[0]
                .getElementsByTag("option")
            //название и адрес обменника, уже отображаемого на странице
            var curName: String? = null
            var curAddress: String? = null
            for (option in optionsOffices) {
                val value = option.attr("value")
                val isSelected = option.hasAttr("selected")
                if (value.isNotEmpty()) {
                    //добавление id обменника в список
                    //разбиение значения option на название и адрес
                    val name = option.text().split("•")[0].trim()
                    val address = "г. ${city.second}, ${option.text().split("•")[1].trim()}"
                    //добавление в список объекта с id, названием и адресом
                    list[city]!!.add(Triple(value, name, address))
                    if (isSelected) {
                        curName = name
                        curAddress = address
                    }
                }
            }
            //получение обменника, уже открытого на странице
            val office = ExchangeOffice(curName!!, curAddress, ajaxDoc.parseExchangeCourses())
            callback(office)
            //проход по обменникам, полученным для данного города
            for (item in list[city]!!) {
                //если обменник еще не был распаршен
                if (item.second != curName) {
                    val lastDoc = loadPage(createUrl(city.first, item.first))
                    val lastOffice = ExchangeOffice(item.second, item.third, lastDoc.parseExchangeCourses())
                    callback(lastOffice)
                }
            }
        }
    }

    override fun loadFilteredExchangeData(
        filter: (name: String) -> Boolean,
        callback: (office: ExchangeOffice) -> Unit
    ) {
        val list = mutableMapOf<Pair<String, String>, ArrayList<Triple<String, String, String>>>()
        val doc = loadPage("https://www.mmbank.by/currency_exchange/")
        //val lines = Files.readAllLines(Paths.get("src/test/resources/mmbtest.html"))
        //val doc = Jsoup.parse(lines.joinToString("\n"))

        //форма для ввода обменника
        val form = doc.getElementsByClass("filter-row")[0]
        //элемент select со списком городов
        val optionsCities = form.getElementsByClass("filter-row__el")[0]
            .getElementsByTag("option")
        for (option in optionsCities) {
            //аттрибут value элемента option
            val value = option.attr("value")
            if (value.isNotEmpty()) {
                list += (value to option.text()) to arrayListOf()
            }
        }
        for (city in list.keys) {
            //запрос формы для каждого города в списке
            val ajaxDoc = loadPage(createUrl(city.first, ""))
            //элемент select со списком обменников
            val optionsOffices = ajaxDoc.getElementsByClass("filter-row__el--mra")[0]
                .getElementsByTag("option")
            //название и адрес обменника, уже отображаемого на странице
            var curName: String? = null
            var curAddress: String? = null
            for (option in optionsOffices) {
                val value = option.attr("value")
                val isSelected = option.hasAttr("selected")
                if (value.isNotEmpty()) {
                    //добавление id обменника в список
                    //разбиение значения option на название и адрес
                    val name = option.text().split("•")[0].trim()
                    val address = "г. ${city.second}, ${option.text().split("•")[1].trim()}"
                    //добавление в список объекта с id, названием и адресом
                    list[city]!!.add(Triple(value, name, address))
                    if (isSelected) {
                        curName = name
                        curAddress = address
                    }
                }
            }
            //получение обменника, уже открытого на странице
            val office = ExchangeOffice(curName!!, curAddress, ajaxDoc.parseExchangeCourses())
            if (filter(curName)) {
                callback(office)
            }
            //проход по обменникам, полученным для данного города
            for (item in list[city]!!) {
                //если обменник еще не был распаршен
                if (item.second != curName && filter(item.second)) {
                    val lastDoc = loadPage(createUrl(city.first, item.first))
                    val lastOffice = ExchangeOffice(item.second, item.third, lastDoc.parseExchangeCourses())
                    callback(lastOffice)
                }
            }
        }
    }

    private fun String.getCourseType() =
        when(this.trim()) {
            "USD" -> CourseType.USD
            "EUR" -> CourseType.EUR
            "RUB" -> CourseType.RUB_100
            "UAH" -> CourseType.UAH_100
            "PLN" -> CourseType.PLN_10
            "CNY" -> CourseType.CNY_10
            else -> CourseType.UNKNOWN
        }
}