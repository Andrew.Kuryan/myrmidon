package logic.web_view_sites

import javafx.concurrent.Worker
import logic.ExchangeOffice
import javafx.scene.web.WebEngine
import logic.AbstractBankSite
import logic.CourseType
import logic.CurrencyCourse
import org.jsoup.Jsoup
import java.util.*
import kotlin.reflect.jvm.reflect

class IdeaBankSite(private val webEngine: WebEngine): AbstractBankSite {

    companion object {
        const val url = "https://www.ideabank.by/o-banke/kursy-valyut/"
    }

    override val name = "Idea Bank"
    //список всех доступных обменников в selectах
    private val list = mutableMapOf<Pair<String, String>, ArrayList<Triple<String, String, String?>>>()
    //очередь задач
    private val taskQueue: Queue<(data: String) -> Unit> = LinkedList()
    private var isRunning = false

    init {
        webEngine.isJavaScriptEnabled = true
        webEngine.setOnAlert {
            if (taskQueue.isNotEmpty()) {
                //запуск очередной задачи
                isRunning = true
                taskQueue.poll()(it.data)
            }
        }
        webEngine.loadWorker.stateProperty().addListener { obs, oldState, newState ->
            println("${webEngine.location}: $newState")
            if (newState == Worker.State.SUCCEEDED) {
                //добавление задачи на переключение вкладки на "отделения"
                taskQueue.offer {
                    val script = """var tab = document.getElementById("oddzialy");
                                    var link = tab.getElementsByTagName("a").item(0);
                                    $(link).trigger("click");"""
                    webEngine.executeScript(script)
                    println("switched")
                }
                //задача парсинга списка идентификторов
                taskQueue.offer {
                    val doc = webEngine.document
                    //элементы с тегом select
                    val selects = doc.getElementsByTagName("select")
                    for (i in 0 until selects.length) {
                        val select = selects.item(i)
                        //поиск элемента с классом kyrs_control city_kyrs
                        if (select.attributes.getNamedItem("class")?.nodeValue == "kyrs_control city_kyrs") {
                            //проход по options найденного select
                            for (f in 0 until select.childNodes.length) {
                                //value option-а
                                val value = select.childNodes.item(f).attributes?.getNamedItem("value")
                                if (value != null && value.textContent.isNotEmpty()) {
                                    //добавление в список пары значение (id) к названию города
                                    list += (value.textContent to select.childNodes.item(f).textContent.trim()) to arrayListOf()
                                }
                            }
                        }
                    }
                    //проход по списку городов
                    for (item in list.keys) {
                        //переключение select городов на каждый город из списка
                        val script = """var city = document.getElementsByClassName("city_kyrs");
                                        city.item(0).value = "${item.first}";
                                        $(city.item(0)).trigger('change');"""
                        webEngine.executeScript(script)
                        for (i in 0 until selects.length) {
                            val select = selects.item(i)
                            //поиск select с классом kyrs_control typ_kyrs
                            if (select.attributes.getNamedItem("class")?.nodeValue == "kyrs_control typ_kyrs") {
                                //проход по option найденного select
                                for (f in 0 until select.childNodes.length) {
                                    val value = select.childNodes.item(f).attributes?.getNamedItem("value")
                                    val address = select.childNodes.item(f).attributes?.getNamedItem("data-address")
                                    val display = select.childNodes.item(f).attributes?.getNamedItem("style")
                                    //если у option задан display:none
                                    if (display != null && display.textContent == "display: none;") {
                                        continue
                                    }
                                    if (value != null && value.textContent.isNotEmpty()) {
                                        //добавление в массив города тройки id обменника, название обменника и адрес
                                        list[item]!!.add(
                                            Triple(
                                                value.textContent,
                                                select.childNodes.item(f).textContent.trim(),
                                                address?.textContent
                                            )
                                        )
                                    }
                                }
                            }
                        }
                    }
                    isRunning = false
                }
                //скрипт, добавляющий слушателя изменения состояния к элементу kyrs_main
                val scriptInit = """var mutationObserver = new MutationObserver(function(mutations) {
                                        if (mutations[0].addedNodes.length !== 0) {
                                            alert('<table class="added">'+mutations[0].addedNodes[0].innerHTML+'</table>');
                                        } else {
                                            alert('<table class="removed">'+mutations[0].removedNodes[0].innerHTML+'</table>');
                                        }
                                    });
                                    mutationObserver.observe(document.getElementsByClassName("kyrs_main").item(0), {
                                        childList: true,
                                    });"""
                webEngine.executeScript(scriptInit)
            }
        }
        isRunning = true
        webEngine.load(url)
    }

    override fun loadAllExchangeData(callback: (office: ExchangeOffice) -> Unit) {
        loadFilteredExchangeData({true}, callback)
    }

    override fun loadFilteredExchangeData(
        filter: (name: String) -> Boolean,
        callback: (office: ExchangeOffice) -> Unit
    ) {
        //если еще есть выполняющиеся задачи
        if (isRunning) {
            return
        }
        val offices = arrayListOf<ExchangeOffice>()
        //основная задача
        taskQueue.offer {
            var k = 0
            //проход по составленному списку идентификаторов
            for ((item, params) in list) {
                //println("${item.first}: ${item.second} = $params -> ${params.size}")
                //проход по списку обменников для каждго города из списка
                for (office in params) {
                    if (!filter(office.second)) {
                        continue
                    }
                    println(office.second)
                    //первая задача в очереди
                    if (k == 0) {
                        //переключение select обменников
                        val task: (data: String) -> Unit =
                            { city: Pair<String, String>, office: Triple<String, String, String?> ->
                                { _ ->
                                    val script = """var city = document.getElementsByClassName("city_kyrs");
                                                city.item(0).value = "${city.first}";
                                                $(city.item(0)).trigger('change');
                                                var point = document.getElementsByClassName("typ_kyrs");
                                                point.item(0).value = "${office.first}";
                                                $(point.item(0)).trigger("change");"""
                                    webEngine.executeScript(script)
                                    val address = "г. ${city.second}, ${office.third}"
                                    offices.add(ExchangeOffice(office.second, address, arrayListOf()))
                                }
                            }(item, office)
                        taskQueue.offer(task)
                        //остальные задачи в очереди
                    } else {
                        //парсинг полученной таблицы из предыдущего запроса и переключение на следующий обменник
                        val task: (data: String) -> Unit =
                            { city: Pair<String, String>, office: Triple<String, String, String?> ->
                                { data: String ->
                                    if (!isRemoveEvent(data)) {
                                        offices[offices.size-1].currencies = parseTable(data)
                                        callback(offices[offices.size-1])
                                        val script = """var city = document.getElementsByClassName("city_kyrs");
                                                city.item(0).value = "${city.first}";
                                                $(city.item(0)).trigger('change');
                                                var point = document.getElementsByClassName("typ_kyrs");
                                                point.item(0).value = "${office.first}";
                                                $(point.item(0)).trigger("change");"""
                                        webEngine.executeScript(script)
                                        val address = "г. ${city.second}, ${office.third}"
                                        offices.add(ExchangeOffice(office.second, address, arrayListOf()))
                                    }
                                }
                            }(item, office)
                        taskQueue.offer(task)
                    }
                    k++
                }
            }
            //задача парсинга последнего обменника
            val task: (data: String) -> Unit =
                { data: String ->
                    if (!isRemoveEvent(data)) {
                        offices[offices.size-1].currencies = parseTable(data)
                        callback(offices[offices.size-1])
                    }
                    isRunning = false
                }
            taskQueue.offer(task)
            //запуск первой задачи из очереди
            taskQueue.poll()("")
        }
        isRunning = true
        //запуск первой задачи из очереди
        taskQueue.poll()("")
        /*
        //список всех доступных обменников в selectах
        val list = mutableMapOf<Pair<String, String>, ArrayList<Triple<String, String, String?>>>()
        //итоговый список обменников
        val offices = mutableListOf<ExchangeOffice>()
        //очередь задач
        val taskQueue: Queue<(data: String) -> Unit> = LinkedList()
        webEngine.isJavaScriptEnabled = true
        webEngine.setOnAlert {
            if (taskQueue.isNotEmpty()) {
                //запуск очередной задачи
                taskQueue.poll()(it.data)
            }
        }
        webEngine.loadWorker.stateProperty().addListener { obs, oldState, newState ->
            println("${webEngine.location}: $newState")
            if (newState == Worker.State.SUCCEEDED) {
                //добавление задачи на переключение вкладки на "отделения"
                taskQueue.offer {
                    val script = """var tab = document.getElementById("oddzialy");
                                    var link = tab.getElementsByTagName("a").item(0);
                                    $(link).trigger("click");"""
                    webEngine.executeScript(script)
                    println("switched")
                }
                //основная задача
                taskQueue.offer {
                    val doc = webEngine.document
                    //элементы с тегом select
                    val selects = doc.getElementsByTagName("select")
                    for (i in 0 until selects.length) {
                        val select = selects.item(i)
                        //поиск элемента с классом kyrs_control city_kyrs
                        if (select.attributes.getNamedItem("class")?.nodeValue == "kyrs_control city_kyrs") {
                            //проход по options найденного select
                            for (f in 0 until select.childNodes.length) {
                                //value optionа
                                val value = select.childNodes.item(f).attributes?.getNamedItem("value")
                                if (value != null && value.textContent.isNotEmpty()) {
                                    //добавление в список пары значение (id) к названию города
                                    list += (value.textContent to select.childNodes.item(f).textContent.trim()) to arrayListOf()
                                }
                            }
                        }
                    }
                    //проход по списку городов
                    for (item in list.keys) {
                        //переключение select городов на каждый город из списка
                        val script = """var city = document.getElementsByClassName("city_kyrs");
                                        city.item(0).value = "${item.first}";
                                        $(city.item(0)).trigger('change');"""
                        webEngine.executeScript(script)
                        for (i in 0 until selects.length) {
                            val select = selects.item(i)
                            //поиск select с классом kyrs_control typ_kyrs
                            if (select.attributes.getNamedItem("class")?.nodeValue == "kyrs_control typ_kyrs") {
                                //проход по option найденного select
                                for (f in 0 until select.childNodes.length) {
                                    val value = select.childNodes.item(f).attributes?.getNamedItem("value")
                                    val address = select.childNodes.item(f).attributes?.getNamedItem("data-address")
                                    val display = select.childNodes.item(f).attributes?.getNamedItem("style")
                                    //если у option задан display:none
                                    if (display != null && display.textContent == "display: none;") {
                                        continue
                                    }
                                    if (value != null && value.textContent.isNotEmpty()) {
                                        //добавление в массив города тройки id обменника, название обменника и адрес
                                        list[item]!!.add(Triple(value.textContent, select.childNodes.item(f).textContent.trim(), address?.textContent))
                                    }
                                }
                            }
                        }
                    }
                    var k = 0
                    //проход по составленному списку идентификаторов
                    for ((item, params) in list) {
                        //println("${item.first}: ${item.second} = $params -> ${params.size}")
                        //проход по списку обменников для каждго города из списка
                        for (office in params) {
                            //первая задача в очереди
                            //println(office.second)
                            if (!filter(office.second)) {
                                continue
                            }
                            if (k == 0) {
                                //переключение select обменников
                                val task: (data: String) -> Unit =
                                    { city: Pair<String, String>, office: Triple<String, String, String?> ->
                                        { _ ->
                                            val script = """var city = document.getElementsByClassName("city_kyrs");
                                                        city.item(0).value = "${city.first}";
                                                        $(city.item(0)).trigger('change');
                                                        var point = document.getElementsByClassName("typ_kyrs");
                                                        point.item(0).value = "${office.first}";
                                                        $(point.item(0)).trigger("change");"""
                                            webEngine.executeScript(script)
                                            val address = "г. ${city.second}, ${office.third}"
                                            offices.add(ExchangeOffice(office.second, address, arrayListOf()))
                                        }
                                    }(item, office)
                                taskQueue.offer(task)
                                //остальные задачи в очереди
                            } else {
                                //парсинг полученной таблицы из предыдущего запроса и переключение на следующий обменник
                                val task: (data: String) -> Unit =
                                    { city: Pair<String, String>, office: Triple<String, String, String?> ->
                                        { data: String ->
                                            if (!isRemoveEvent(data)) {
                                                offices[offices.size-1].currencies = parseTable(data)
                                                callback(offices[offices.size-1])
                                                val script = """var city = document.getElementsByClassName("city_kyrs");
                                                        city.item(0).value = "${city.first}";
                                                        $(city.item(0)).trigger('change');
                                                        var point = document.getElementsByClassName("typ_kyrs");
                                                        point.item(0).value = "${office.first}";
                                                        $(point.item(0)).trigger("change");"""
                                                webEngine.executeScript(script)
                                                val address = "г. ${city.second}, ${office.third}"
                                                offices.add(ExchangeOffice(office.second, address, arrayListOf()))
                                            }
                                        }
                                    }(item, office)
                                taskQueue.offer(task)
                            }
                            k++
                        }
                    }
                    //задача парсинга последнего обменника
                    val task: (data: String) -> Unit =
                        { data: String ->
                            if (!isRemoveEvent(data)) {
                                offices[offices.size-1].currencies = parseTable(data)
                                callback(offices[offices.size-1])
                            }
                        }
                    taskQueue.offer(task)
                    //запуск первой задачи из очереди
                    taskQueue.poll()("")
                }
                //скрипт, добавляющий слушателя изменения состояния к элементу kyrs_main
                val scriptInit = """var mutationObserver = new MutationObserver(function(mutations) {
                                        if (mutations[0].addedNodes.length !== 0) {
                                            alert('<table class="added">'+mutations[0].addedNodes[0].innerHTML+'</table>');
                                        } else {
                                            alert('<table class="removed">'+mutations[0].removedNodes[0].innerHTML+'</table>');
                                        }
                                    });
                                    mutationObserver.observe(document.getElementsByClassName("kyrs_main").item(0), {
                                        childList: true,
                                    });"""
                webEngine.executeScript(scriptInit)
            }
        }
        webEngine.load(url)
        */
    }

    private fun isRemoveEvent(data: String): Boolean {
        val doc = Jsoup.parse(data)
        return doc.getElementsByTag("table")[0].attr("class") == "removed"
    }

    private fun parseTable(table: String): ArrayList<CurrencyCourse> {
        val courses = arrayListOf<CurrencyCourse>()
        val doc = Jsoup.parse(table)
        val rows = doc.getElementsByTag("tbody")[0]
            .getElementsByTag("tr")
        for ((i, row) in rows.withIndex()) {
            if (row.attr("class") != "border") {
                val cells = row.getElementsByTag("td")
                if (cells.size >= 3) {
                    courses += CurrencyCourse(cells[0].text().getCourseType(),
                                              cells[1].text().toDouble(),
                                              cells[2].text().toDouble())
                }
            } else if (i != 0) {
                break
            }
        }
        return courses
    }

    private fun String.getCourseType() =
        when(this.trim()) {
            "1 USD" -> CourseType.USD
            "1 EUR" -> CourseType.EUR
            "100 RUB" -> CourseType.RUB_100
            "10 PLN" -> CourseType.PLN_10
            else -> CourseType.UNKNOWN
        }
}