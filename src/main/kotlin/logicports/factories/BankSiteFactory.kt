package logicports.factories

import javafx.scene.web.WebView
import logic.AbstractBankSite
import logic.parsing_sites.BNBSite
import logic.parsing_sites.FransabankSite
import logic.parsing_sites.RRBSite
import logic.web_view_sites.IdeaBankSite
import logic.parsing_sites.MMBankSite

interface BankSiteFactory {

    fun createBankSite(): AbstractBankSite
}

class BNBSiteFactory: BankSiteFactory {

    override fun createBankSite() = BNBSite()
}

class FransabankSiteFactory: BankSiteFactory {

    override fun createBankSite() = FransabankSite()
}

class RRBSiteFactory: BankSiteFactory {

    override fun createBankSite() = RRBSite()
}

class MMBSiteFactory: BankSiteFactory {

    override fun createBankSite() = MMBankSite()
}

class IdeaBankSiteFactory: BankSiteFactory {

    private val webView = WebView()

    override fun createBankSite(): AbstractBankSite {
        return IdeaBankSite(webEngine = webView.engine)
    }
}