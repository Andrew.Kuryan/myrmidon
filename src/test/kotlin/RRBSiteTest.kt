import logic.MODE
import logic.parsing_sites.RRBSite
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.lang.StringBuilder

@DisplayName("RRB site page")
class RRBSiteTest {

    val outputAll = """ExchangeOffice(name=Минск, Головной офис ЗАО «РРБ-Банк», address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Минск, ПБУ №7/1, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Минск, ЦБУ №7, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Молодечно, ПБУ №2, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Гомель, ЦБУ №3, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Гомель, Пункт обмена валют №3/1 ЦБУ №3, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Гродно, ЦБУ №4, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Бобруйск, ЦБУ №6/1, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Витебск, ЦБУ № 2, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Брест, ЦБУ №1, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])
ExchangeOffice(name=Гомель, Пункт обмена валют №3/2 ЦБУ№3, address=null, currencies=[CurrencyCourse(type=USD, purchase=2.122, selling=2.133), CurrencyCourse(type=EUR, purchase=2.375, selling=2.4), CurrencyCourse(type=RUB_100, purchase=3.26, selling=3.29)])

    """.trimIndent()

    @BeforeEach
    fun setTestMode(){
        MODE = "TEST"
    }

    @AfterEach
    fun setDevMode() {
        MODE = "DEV"
    }

    @Test
    @DisplayName("should parse all exchange offices on page")
    fun parseAll() {
        val sb = StringBuilder()
        RRBSite().loadAllExchangeData {
            //println(it)
            sb.append(it.toString()).append("\n")
        }
        //println(sb)
        assertEquals(outputAll, sb.toString(), "Parse data is not correct")
    }
}