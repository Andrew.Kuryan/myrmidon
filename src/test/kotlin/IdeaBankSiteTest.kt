import javafx.application.Application
import javafx.scene.web.WebView
import javafx.stage.Stage
import logic.web_view_sites.IdeaBankSite
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Idea bank site page")
class IdeaBankSiteTest: Application() {

    override fun start(primaryStage: Stage?) {
        val webView = WebView()

        IdeaBankSite(webView.engine).loadAllExchangeData {
            println(it)
        }
    }

    @Test
    @DisplayName("should parse all available exchange offices")
    fun parseAll() {

    }
}