import logic.MODE
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

class MMBSiteTest {

    @BeforeEach
    fun setTestMode(){
        MODE = "TEST"
    }

    @AfterEach
    fun setDevMode() {
        MODE = "DEV"
    }

    /*@Test
    @DisplayName("should parse all available exchange offices")
    fun parseAll() {
        val sb = StringBuilder()
        MMBankSite().loadAllExchangeData {
            sb.append(it.toString()).append("\n")
            //println(it)
        }
    }*/
}